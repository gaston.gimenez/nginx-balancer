#!/bin/bash

if [ $UID -ne 0 ]; then
echo "Ejecute este script como 'root' o 'sudo ./$0'"
exit 1
fi

# hostname

read -p "hostname: " host

hostnamectl set-hostname $host

# actualizamos el sistema
apt update && apt upgrade -y || yum update || dnf update

# intalar nginx

apt install nginx -y || yum intall nginx || dnf install nginx
